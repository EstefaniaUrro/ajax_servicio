var xmlhttp;
var txtusuario;
var txtcontraseña;
var tabla = [{ usuario: "user", contraseña: "password" }];
window.onload = function () {
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    txtusuario = document.getElementById("usuarios");
    txtcontraseña = document.getElementById("contraseña");
    llamada();
}

function llamada() {
    xmlhttp.open('post', 'Servicios', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.onreadystatechange = accionesAjaxState;
    //      xmlhttp.setRequestHeader('Access-Control-Allow-Headers', '*');
    // 	xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    // 	xmlhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
    xmlhttp.send('nombre=' + txtusuario.value + '&correo=' + txtcontraseña.value);
}

function accionesAjaxState() {
    if (this.status === 200 && this.readyState === 4) {
        alert(this.responseText);
        var objJSON = JSON.parse(this.responseText);
        function nuevo(e) {
            e.preventDefault();
            var nuevoR = [objJSON.nombre, objJSON.correo];
            tabla.push();
        }
        var c = document.getElementById('contenidorespuesta');
        c.innerHTML = '<span>Nombre:' + objJSON.nombre + '</span><span> Correo:' + objJSON.correo + '</span>';
    } else if (this.readyState === 4) {
        alert('Elemento no Disponible');
        return;
    }
    cargarevento();
}

function cargarevento() {
    document.getElementById("mostrar-tabla").addEventListener("click", mostrartabla, false);
    document.getElementById("nuevaTabla").addEventListener("submit", nuevaTabla, false);
}

function mostrartabla() {
    var ctabla = document.getElementById("lista");
    var tablaLlena = " ";

    for (var i = 0; i < tabla.length; i++) {
        tablaLlena += "<tr><td>" + tabla[i].usuario + "</td><td>" + tabla[i].contraseña + "</td></tr>";
    }
    ctabla.innerHTML = tablaLlena;
}

function nuevaTabla(e) {
    e.preventDefault();
    var usuariosdatos = document.getElementById("usuarios").value;
    var contraseñadatos = document.getElementById("contraseña").value;
    var nuevaTabla = { usuario: usuariosdatos, contraseña: contraseñadatos };
    tabla.push(nuevaTabla);
}    